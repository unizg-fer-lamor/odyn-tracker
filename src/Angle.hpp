#ifndef ANGLE_H
#define ANGLE_H

#include <math.h>

// Class created to handle angular data

class Angle
{
public:
  Angle();
  ~Angle();

  // subtraction of angular values (assures [-pi, pi])
  float subtract(const float a, const float b);
  double subtract(const double a, const double b);

  float radToDeg(const float a);
  double radToDeg(const double a);

  float degToRad(const float a);
  double degToRad(const double a);

  // assure that angle is in [-pi, pi]
  float assure180(const float a);
  double assure180(const double a);
};

#endif /* ANGLE_H */
