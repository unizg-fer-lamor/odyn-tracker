#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/opencv.hpp>

#include "Sphere.hpp"
#include "Camera.hpp"
#include "Odometry.hpp"

class Features
{
private:
  Sphere sph_;
public:
  std::vector<KeyPoint> pts_detected_;
  std::vector<bool> match_static_;
  std::vector<Point2f> prev_pts_;
  std::vector<Point2f> next_pts_;

  std::vector<Point3d> aux_points_;
  Point3d aux_init_, aux_terminal_;
  double aux_mindist_;
  unsigned long stamp_prev_, stamp_curr_;

  Features();

  ~Features();

  void detect(const Mat& image, const Mat& mask, const bool robot_moving);

  void detectMovement(Camera& cam, Odometry& odom);
};
