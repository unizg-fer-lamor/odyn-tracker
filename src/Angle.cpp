#include "Angle.hpp"

// Class created to handle angular data

Angle::Angle()
{
}

Angle::~Angle()
{
}

float Angle::subtract(const float a, const float b)
{
  float diff = fmod((a - b) + (float) M_PI, 2.0f * (float) M_PI) - (float) M_PI;
  return this->assure180(diff);
}

double Angle::subtract(const double a, const double b)
{
  double diff = (double) (fmod(((float) a - (float) b) + (float) M_PI, 2.0f * (float) M_PI) - (float) M_PI);
  return this->assure180(diff);
}

float Angle::radToDeg(const float a)
{
  return a * 180.0f / (float) M_PI;
}

double Angle::radToDeg(const double a)
{
  return a * 180.0d / M_PI;
}

float Angle::degToRad(const float a)
{
  return a * (float) M_PI / 180.0f;
}

double Angle::degToRad(const double a)
{
  return a * M_PI / 180.0d;
}

float Angle::assure180(const float a)
{
  float angle = fmod(a, 2.0f * (float) M_PI);
  if (angle > M_PI)
    return angle - 2.0f * (float) M_PI;
  else if (angle <= -M_PI)
    return angle + 2.0f *(float) M_PI;
  else return angle;
}

double Angle::assure180(const double a)
{
  double angle = (double) fmod((float) a, 2.0f * (float) M_PI);
  if (angle > M_PI)
    return angle - 2.0d * M_PI;
  else if (angle <= -M_PI)
    return angle + 2.0d * M_PI;
  else return angle;
}
