# include "FlowVector.hpp"

FlowVector::FlowVector(Point3d init, Point3d terminal)
{
  this->init_ = init;
  this->terminal_ = terminal;
  this->is_static_ = true;
}

FlowVector::FlowVector(Point img_init, Point img_terminal, const Camera cam, bool is_static)
{
  this->img_init_ = img_init;
  this->img_terminal_ = img_terminal;
  this->init_ = cam.cam2Sphere(img_init);
  this->terminal_ = cam.cam2Sphere(img_terminal);
  this->is_static_ = is_static;
}

FlowVector::~FlowVector()
{
}

double FlowVector::getAzimuth() const
{
  double dx, dy;
  dx = terminal_.x - init_.x;
  dy = terminal_.y - init_.y;

  return atan2(dy, dx);
}

double FlowVector::getElevation() const
{
  double dz;
  dz = terminal_.z;

  return acos(dz);
}

double FlowVector::getChordDist() const
{
  return sqrt(pow(terminal_.x - init_.x,2) + pow(terminal_.y - init_.y,2) + pow(terminal_.z - init_.z,2));
}

double FlowVector::getGcDist() const
{
  double ch;
  ch = sqrt(pow(terminal_.x - init_.x,2) + pow(terminal_.y - init_.y,2) + pow(terminal_.z - init_.z,2));

  return 2.0d * asin(ch / 2.0d);
}

Point3d SumFlowVectorAtId::operator() (Point3d acc, const FlowVector& fv)
{
  if (query_id_ == fv.id_)
  {
    return acc + fv.terminal_;
  }
  else
  {
    return acc;
  }
}


bool FlowVectorAtId::operator() (const FlowVector& fv)
{
  return (fv.id_ == query_id_);
}

bool AreEqualFlowVectors::operator() (const FlowVector& fv1, const FlowVector& fv2)
{
  double inter_gcdist;
  float inter_azimuth_diff, inter_elevation_diff;

  inter_gcdist = sph_.grtCircleDist(fv1.terminal_, fv2.terminal_);

  inter_azimuth_diff = ang_.subtract(fv2.getAzimuth(), fv1.getAzimuth());
  inter_elevation_diff = ang_.subtract(fv2.getElevation(), fv1.getElevation());

  float angle_azimuth_thresh = (float) M_PI / 60.0f;
  float angle_elev_thresh = (float) M_PI / 10.0f; // willing to tolerate greater difference in elevetion
  double dist_thresh = M_PI / 15.0d;

  if (fabs(inter_azimuth_diff) < angle_azimuth_thresh && fabs(inter_elevation_diff) < angle_elev_thresh &&
      inter_gcdist < dist_thresh)
  {
    return true;
  }
  else
  {
    return false;
  }
}
