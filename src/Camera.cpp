#include "Camera.hpp"

// class created for storing calibration parameters and pixel, sphere and world transformations

Camera::Camera()
{
  // camera calibration parameters are hardcoded in constructor (should resolve this better)
  // Calibration parameters from Bertrand
  //    K_ = (Mat_<double>(3,3) << 682.89101, 0.0, 325.3535329, 0.0, 679.4930751, 241.3088672, 0.0, 0.0, 1.0);
  //    xi_ = 3.018261383;
  K_ = (Mat_<double>(3,3) << 668.62768, 0.0, 323.96689, 0.0, 668.69880, 254.02298, 0.0, 0.0, 1.0);
  xi_ = 2.96269;
  if (determinant(K_) != 0)
  {
    Kinv_ = K_.inv();
  }
  else
  {
    ROS_ERROR("Camera intrinsic matrix is singular! How is this possible!?");
  }
}

Camera::~Camera()
{
}

Point3d Camera::cam2Sphere(const Point& pt) const
{
  // Lift homogenous coordinates of the point on the image to the unit sphere
  Mat m = (Mat_<double>(3,1) << (double) pt.x, (double) pt.y, 1.0d);
  m = Kinv_ * m;
  double x = m.at<double>(0,0), y = m.at<double>(1,0), rhe;
  rhe = (xi_ + sqrt(1.0d + (1.0d - pow(xi_,2)) * (pow(x,2) + pow(y,2)))) / (pow(x,2) + pow(y,2) + 1.0d);
  m.at<double>(0,0) = x * rhe;
  m.at<double>(1,0) = y * rhe;
  m.at<double>(2,0) = rhe - xi_;

  return Point3d(m.at<double>(0,0), m.at<double>(1,0), m.at<double>(2,0));
}

Point3d Camera::world2Sphere(const Point3d& pt) const
{
  // Normalize the 3D vector to unit sphere
  double norm = sqrt(pow(pt.x,2) + pow(pt.y,2) + pow(pt.z,2));
  Point3d pt_norm;
  pt_norm.x = pt.x / norm; pt_norm.y = pt.y / norm; pt_norm.z = pt.z / norm;
  return pt_norm;
}

Point Camera::sphere2Cam(const Point3d& pt)
{
  // Project the point on the sphere onto the image plane
  double norm_pt = norm(pt);
  Mat pt_aux = (Mat_<double>(3,1) << pt.x / (pt.z + xi_ * norm_pt), pt.y / (pt.z + xi_ * norm_pt), 1.0d);
  pt_aux = K_ * pt_aux;

  return Point((float) pt_aux.at<double>(0,0), (float) pt_aux.at<double>(0,1));
}
