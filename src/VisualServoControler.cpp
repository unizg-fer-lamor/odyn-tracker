#include "VisualServoControler.hpp"

double VisualServoControler::calculateAdaptGain(double err)
{
  return a_ * exp(-b_ * fabs(err)) + c_;
}

VisualServoControler::VisualServoControler()
{
  v_ = 0.0d;
  omega_ = 0.0d;
  elev_des_ = ang_.degToRad(90.0d);
  azim_des_ = ang_.degToRad(0.0d);
  activated_ = false;
}

VisualServoControler::~VisualServoControler()
{
}

void VisualServoControler::setDesiredPosition(double elevation, double azimuth)
{
  elev_des_ = elevation;
  azim_des_ = azimuth;
}

void VisualServoControler::activate(bool flag)
{
  activated_ = flag;
  v_ = 0.0d;
  omega_ = 0.0d;
}

void VisualServoControler::parametrizeControler(double arb_depth, double l_0, double l_inf, double l_der)
{
  arb_depth_ = arb_depth;

  // calculate the adaptive gain parameters
  lambda_0_ = l_0; lambda_inf_ = l_inf; lambda_der_ = l_der;
  a_ = lambda_0_ - lambda_inf_;
  b_ = lambda_der_ / a_;
  c_ = lambda_inf_;

}

void VisualServoControler::safetyCheck(std::vector<Point3d> Zk)
{
  // safety check and limit
  if (isnan(v_) || isnan(omega_) || isinf(v_) || isinf(omega_))
    v_ = 0.0d; omega_ = 0.0d;

  if (fabs(v_) > 0.5)
    v_ = copysign(0.5d, v_);

  if (fabs(omega_) > 0.5d)
    omega_ = copysign(0.5d, omega_);

  if(!activated_)
    v_ = 0.0d; omega_ = 0.0d;
}

void VisualServoControler::calculateControlVector(Point3d est_u, Camera& cam)
{
  Point3d des_u = Point3d(sin(elev_des_) * cos(azim_des_), sin(azim_des_) * sin(elev_des_), cos(elev_des_));
  Point2d pt_est = cam.sphere2Cam(est_u),
      pt_des = cam.sphere2Cam(des_u);

  // Center of the image is the projection of the north pole
  Point2d center = cam.sphere2Cam(Point3d(0.0d, 0.0d, 1.0d));
  double x0 = center.x, y0 = center.y;

  double azim_est = atan2(est_u.y, est_u.x),
      rho_est = sqrt(pow(pt_est.x - x0,2) + pow(pt_est.y - y0,2));
  double rho_des = sqrt(pow(pt_des.x - x0,2) + pow(pt_des.y - y0,2));

  // avoiding numerical issues when azim close to zero or -/+ pi (division by sin(azim))
  double thresh = 15.0d;
  if (fabs(azim_est) < ang_.degToRad(thresh))
    azim_est = copysign(ang_.degToRad(thresh), azim_est);
  if (fabs(ang_.subtract(azim_est, M_PI)) < ang_.degToRad(thresh) ||
      fabs(ang_.subtract(azim_est, -M_PI)) < ang_.degToRad(thresh))
    azim_est = copysign(ang_.degToRad(180.0d - thresh), azim_est);

  // calculate the adaptive gain based on the great circle distance between desired and estimated position
  double err = sph_.grtCircleDist(est_u, des_u);
  gain_ = calculateAdaptGain(err);

  double depth = arb_depth_;
  Mat interaction_mat = (Mat_<double>(2,2) <<
                         -sin(azim_est) / depth, 0.0d,
                         -cos(azim_est) / (depth * rho_est), -1.0d);
  Mat error_mat = (Mat_<double>(2,1) <<
                   -gain_ * (rho_est - rho_des),
                   -gain_ * ang_.subtract(azim_est, azim_des_));
  Mat control_vec = Mat(2, 1, CV_64F);

  // control vector
  interaction_mat = interaction_mat.inv(DECOMP_SVD);
  control_vec = interaction_mat * error_mat;
  v_ = control_vec.at<double>(0,0);
  omega_ = control_vec.at<double>(1,0);

  //    if (v_ < 0.1d)
  //      omega_ = 0.8d * ang_.subtract(azim_est, azim_des_);
  //    v_ = gain_ * depth * ang_.subtract(elev_est, elev_des_) / (sin(azim_est) * cos(elev_est));
  //    omega_ = -v_ * cos(azim_est) / (depth * sin(elev_est)) + gain_ * ang_.subtract(azim_est, azim_des_);
}

double VisualServoControler::getTransVel()
{
  return v_;
}

double VisualServoControler::getRotVel()
{
  return omega_;
}

Point3d VisualServoControler::getDesiredPosition()
{
  Point3d pt_desired;
  pt_desired.x = sin(elev_des_) * cos(azim_des_);
  pt_desired.y = sin(elev_des_) * sin(azim_des_);
  pt_desired.z = cos(elev_des_);

  return pt_desired;
}
