#include "BayesianFilter.hpp"

double BayesianFilter::pdfVMF(Point3d Zk, double k, Point3d x)
{
  if (k == 0.0d)
    return 1.0d / (4.0d * M_PI);
  else
    return k * exp(k * (Zk.dot(x) - 1.0d)) / (2.0d*M_PI * (1.0d - exp(-2.0d * k)));
}

Mat BayesianFilter::sampleVMF(int n, Point3d u, double k)
{
  // method for sampling n points from the von Mises-Fisher distribution
  Mat ksi = Mat(n, 1, CV_64F), w = Mat(n, 1, CV_64F);
  time_t t = time(NULL);
  RNG rng(t);
  rng.fill(ksi, RNG::UNIFORM, (double) 0, (double) 1);
  // calculate inversion for distribution of w
  // 1 + (log(ksi) + log(1 - exp(-2*k)*(ksi-1)/ksi)) / k
  subtract(ksi, 1.0d, w); divide(w, ksi, w);
  w = w * exp(-2.0d * k);
  subtract(1.0d, w, w);
  log(w,w); log(ksi, ksi);
  add(ksi, w, w);
  divide(w, k, w);
  add(1.0d, w, w);

  Mat v = Mat(n, 1, CV_64F), v_cos = Mat(n, 1, CV_64F), v_sin = Mat(n, 1, CV_64F);
  t = time(NULL);
  rng(t);
  rng.fill(v, RNG::UNIFORM, (double) 0, (double) 1);
  v = 2.0d * M_PI * v;
  for (int i = 0; i < (int) v.rows; i++)
  {
    v_cos.at<double>(i,0) = cos(v.at<double>(i,0));
    v_sin.at<double>(i,1) = sin(v.at<double>(i,0));
  }

  // calculate ksi = sqrt(1 - w.^2)
  pow(w, 2.0d, ksi);
  subtract(1.0d, ksi, ksi);
  sqrt(ksi, ksi);
  multiply(ksi, v_cos, v_cos);
  multiply(ksi, v_sin, v_sin);

  Mat samples = Mat(n, 4, CV_64F);
  std::vector<Mat> aux;
  aux.push_back(v_cos);
  aux.push_back(v_sin);
  aux.push_back(w);
  aux.push_back(Mat::ones(n, 1, CV_64F));
  hconcat(aux, samples);
  transpose(samples, samples);

  // the samples are around (0,0,1) and we need to rotate them to be around u
  double phi = atan2(u.y, u.x), theta = acos(u.z);
  Mat R;
  // rotation around y axis
  R = (Mat_<double>(4,4) <<
       cos(theta), 0.0d, sin(theta), 0.0d,
       0.0d, 1.0d, 0.0d, 0.0d,
       -sin(theta), 0.0d, cos(theta), 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  samples = R * samples;
  // rotation around z axis
  R = (Mat_<double>(4,4) <<
       cos(phi), -sin(phi), 0.0d, 0.0d,
       sin(phi), cos(phi), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  samples = R * samples;

  return samples;
}

double BayesianFilter::A(double k)
{
  return 1 / tanh(k) - 1 / k;
}

double BayesianFilter::dA(double k)
{
  double csch = 2.0d / (exp(k) - exp(-k));
  return 1 / (k*k) - csch*csch;
}

double BayesianFilter::Ainv(double y, double guess)
{
  // Initial guess
  double x = guess, residual = 0.0d, deriv;
  // Invert using Newton’s method
  do {
    residual = A(x)-y, deriv = dA(x);
    x -= residual/deriv;
  } while (fabs((float) residual) > 1e-5f);

  return x;
}

double BayesianFilter::convolveVMF(double k1, double k2)
{
  return Ainv(A(k1) * A(k2), min(k1, k2));
}

BayesianFilter::BayesianFilter()
{
  initialized_ = false;
}

BayesianFilter::~BayesianFilter()
{
}

void BayesianFilter::initialize(Point3d u, double k)
{
  sample_num_ = 300;
  initialized_ = true;
  samples_ = sampleVMF(sample_num_, u, k);
  est_u_ = u;
  est_k_ = k;
}

void BayesianFilter::predict(Odometry& odom, double p_noise_k, vector<Point3d> Zk, double dt)
{
  // calculate the rotational speed of the object
  double dth, object_th = 0.0d;
  if (!rot_velocities_.empty())
  {
    double mean_rot_vel = 0.0d;
    mean_rot_vel = accumulate(rot_velocities_.begin(), rot_velocities_.end(), mean_rot_vel, std::plus<double>());
    mean_rot_vel = mean_rot_vel / (double) rot_velocities_.size();

    // calculate the rotation needed due to objects movement
    object_th = ang_.assure180(mean_rot_vel * dt);
  }

  // transform the point as if it was on a fixed distance
  double scale = 0.8;
  est_u_ = odom.tfToCurrentFrame(scale * est_u_);
  dth = object_th;

  Mat R;
  R = (Mat_<double>(4,4) <<
       cos(dth), -sin(dth), 0.0d, 0.0d,
       sin(dth), cos(dth), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  Mat u_hom = (Mat_<double>(4,1) << est_u_.x, est_u_.y, est_u_.z, 1.0d);
  u_hom = R * u_hom;
  est_u_.x = u_hom.at<double>(0,0);
  est_u_.y = u_hom.at<double>(1,0);
  est_u_.z = u_hom.at<double>(2,0);

  // keep the size of the velocity vector bounded
  if (rot_velocities_.size() > 5)
    rot_velocities_.erase(rot_velocities_.begin());
  if(Zk.empty() && !rot_velocities_.empty())
  {
    // when no measurements remove the oldest velocity
    rot_velocities_.erase(rot_velocities_.begin());
  }
  // convolution with process noise vMF
  est_k_ = convolveVMF(est_k_, p_noise_k);

  samples_ = sampleVMF(sample_num_, est_u_, est_k_);
}

void BayesianFilter::update(vector<Point3d> Zk_vec, double k, double dt)
{
  Point3d Zk, u_prev = est_u_;
  double min_dist = 2.0d * M_PI, dot_dist;
  int min_idx = 0;
  if (!Zk_vec.empty())
  {
    // Find the closest measurement to the current object using angle between vectors
    for (int i = 0; i < (int) Zk_vec.size(); i++)
    {
      //        log_map_dist = sqrt(pow(log(est_k_ / k),2) + pow(acos(est_u_.dot(Zk_vec.at(i))), 2));
      dot_dist = acos(est_u_.dot(Zk_vec.at(i)));
      if (dot_dist < min_dist)
      {
        min_dist = dot_dist;
        min_idx = i;
      }
    }
    // If there are no measurements close to the state
    if (min_dist > ang_.degToRad(25.0d))
      return;

    Zk = Zk_vec[min_idx];
    double k_prod = sqrt(pow(est_k_,2) + pow(k,2) + 2.0d * est_k_ * k * est_u_.dot(Zk));
    est_u_ = (est_k_ * est_u_ + k * Zk);
    est_u_ = est_u_ * (1.0d / k_prod);
    est_k_ = k_prod;

    // calculate rotational velocity
    double omega;
    omega = ang_.subtract(atan2(est_u_.y, est_u_.x), atan2(u_prev.y, u_prev.x)) / dt;
    rot_velocities_.push_back(omega);
  }
}
