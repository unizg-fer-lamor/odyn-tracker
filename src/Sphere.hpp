#ifndef SPHERE_H
#define SPHERE_H

#include <opencv2/core/core.hpp>

// Class created to handle some distances on the sphere

using namespace cv;

class Sphere
{
public:
  Sphere();
  ~Sphere();

  // great circle distance between two points on the sphere
  float grtCircleDist(const Point3f a, const Point3f b);
  double grtCircleDist(const Point3d a, const Point3d b);

  // chord distance, i.e. euclideian distance
  float chordDist(const Point3f a, const Point3f b);
  double chordDist(const Point3d a, const Point3d b);
};

#endif /* SPHERE_H */
