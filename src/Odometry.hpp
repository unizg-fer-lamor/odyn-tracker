#ifndef ODOMETRY_H
#define ODOMETRY_H

#include <vector>
#include <nav_msgs/Odometry.h>
#include <opencv2/core/core.hpp>

#include "Angle.hpp"

using namespace cv;

// class for wrapping and handeling odometry data

class Odometry
{
private:
  double vel_x_, vel_th_;
  Angle ang_;
public:
  double x_prev_, y_prev_, z_prev_, th_prev_;
  double x_curr_, y_curr_, z_curr_, th_curr_;
  unsigned long stamp_prev_, stamp_curr_;
  bool robot_is_moving_;
  std::vector<nav_msgs::Odometry> stack_;

  Odometry();
  ~Odometry();

  // set all variables to desired values
  void initialize();
  // stack the new odometry and interpolate based on the time stamp
  void update(const unsigned long img_stamp);

  // frame transforms for points on the sphere
  Point3d tfToCurrentFrame(Point3d pt);
  Point3d justRotateForDisplacement(Point3d pt);
};

#endif /* ODOMETRY_H */
