#ifndef CAMERA_H
#define CAMERA_H

#include <opencv2/core/core.hpp>
#include <ros/ros.h>

using namespace cv;

class Camera
{
public:
  Mat K_;
  Mat Kinv_;
  double xi_;

  Camera();
  ~Camera();

  Point3d cam2Sphere(const Point& pt) const;

  Point3d world2Sphere(const Point3d& pt) const;

  Point sphere2Cam(const Point3d& pt);
};

#endif /* CAMERA_H */
