#include "Sphere.hpp"

// Class created to handle some distances on the sphere

Sphere::Sphere()
{
}

Sphere::~Sphere()
{
}

float Sphere::grtCircleDist(const Point3f a, const Point3f b)
{
  float ch;
  ch = sqrt(pow(b.x - a.x,2) + pow(b.y - a.y,2) + pow(b.z - a.z,2));

  return 2.0f * asin(ch / 2.0f);
}

double Sphere::grtCircleDist(const Point3d a, const Point3d b)
{
  double ch;
  ch = sqrt(pow(b.x - a.x,2) + pow(b.y - a.y,2) + pow(b.z - a.z,2));

  return 2.0d * asin(ch / 2.0d);
}

float Sphere::chordDist(const Point3f a, const Point3f b)
{
  return sqrt(pow(b.x - a.x,2) + pow(b.y - a.y,2) + pow(b.z - a.z,2));
}

double Sphere::chordDist(const Point3d a, const Point3d b)
{
  return sqrt(pow(b.x - a.x,2) + pow(b.y - a.y,2) + pow(b.z - a.z,2));
}

