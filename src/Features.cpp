#include "Features.hpp"

Features::Features()
{
}

Features::~Features()
{
}

void Features::detect(const Mat& image, const Mat& mask, const bool robot_moving)
{
  // detect the features with some method and store them in the right container
  //    int nfeatures = 1250, nlevels = 8, edge_threshold = 11,
  //        first_level = 0, WTA_K = 2, patch_size = 31;
  //    float scale_factor = 1.2;

  //    ORB detector(nfeatures, scale_factor, nlevels,
  //                 edge_threshold,
  //                 first_level,
  //                 WTA_K,
  //                 ORB::FAST_SCORE,
  //                 patch_size);
  //    // detect and describe features at the same time
  //    detector(image, mask_, ftrs_.pts_detected_, noArray());

  int maxCorners = robot_moving ? 700 : 700, blockSize=3;
  double qualityLevel=0.001, k=0.04, minDistance=3.;
  bool useHarrisDetector=false;

  GoodFeaturesToTrackDetector detector(maxCorners, qualityLevel, minDistance, blockSize, useHarrisDetector, k);
  detector.detect(image, pts_detected_, mask);
}

void Features::detectMovement(Camera& cam, Odometry& odom)
{
  // apply robots movement and see if the matched feature is close to the great arc. If so then the feature is static.
  match_static_.clear();
  match_static_.assign(prev_pts_.size(), false);
  // Preparing matrices for k nearests
  Mat train_data(prev_pts_.size(),3,CV_32FC1), train_classes;
  train_classes = Mat::zeros(prev_pts_.size(), 1, CV_32FC1);

  Point pt_img, pt_matched_img;
  Point3d pt, pt_matched, e1, e2, qmin, pt_aux1, pt_aux2;
  double min_gcdist, feat_static_thresh;
  bool in_lune;

  // When the robot is static we can have a more strict threshold
  feat_static_thresh = odom.robot_is_moving_ ? M_PI / 180.0d : M_PI / 180.0d;

  for (int i = 0; i < (int) prev_pts_.size(); i++)
  {
    pt_img = prev_pts_.at(i);
    pt = cam.cam2Sphere(pt_img);
    pt_matched_img = next_pts_.at(i);
    pt_matched = cam.cam2Sphere(pt_matched_img);

    // later we will analyze k nearests of the matched features
    train_data.at<float>(i,0) = (float) pt_matched.x;
    train_data.at<float>(i,1) = (float) pt_matched.y;
    train_data.at<float>(i,2) = (float) pt_matched.z;

    // calculated the points defining the great arc
    e1 = odom.justRotateForDisplacement(pt); // this amounts to point in infinity (only rotation required)
    e2 = odom.tfToCurrentFrame(pt);

    // calculate minimum distance of the matched point to great circle(e1,e2)
    qmin = e1.dot(pt_matched) * e1 + e2.dot(pt_matched) * e2;

    // If qmin is the pole of the arc then it will project to sphere origin
    if (norm(qmin) > 10e-10) // should be zero but to account for numerical error
    {
      qmin = qmin * (1.0d / norm(qmin));
      min_gcdist = sph_.grtCircleDist(qmin, pt_matched);
    }
    else
    {
      min_gcdist = M_PI / 2.0d;
    }

    // Check if qmin is in the lune of (e1,e2)
    pt_aux1 = e1.cross(qmin);
    pt_aux2 = qmin.cross(e2);
    in_lune = false;
    if (pt_aux1.dot(pt_aux2) > 0.0d)
      in_lune = true;

    pt_aux2 = e1.cross(e2);
    if (pt_aux1.dot(pt_aux2) > 0.0d)
    {
      in_lune = in_lune && true;
    }
    else
    {
      in_lune = false;
    }

    if (!in_lune)
    {
      min_gcdist = min(sph_.grtCircleDist(e1, pt_matched), sph_.grtCircleDist(e2, pt_matched));
    }

    // Decide if the feature is static
    if (min_gcdist < feat_static_thresh)
    {
      match_static_.at(i) = true;
      train_classes.at<float>(i,0) = 1.0f;
    }
  }

  // Examine the neighbourhood of the features
  const int K = 7;
  bool response[prev_pts_.size()];
  CvKNearest knn(train_data, train_classes, Mat(), false, K);

  Mat sample(1, 3, CV_32FC1);
  std::vector<int> erase_at;
  for (int i = 0; i < (int) prev_pts_.size(); i++)
  {
    sample.at<float>(0,0) = train_data.at<float>(i,0);
    sample.at<float>(0,1) = train_data.at<float>(i,1);
    sample.at<float>(0,2) = train_data.at<float>(i,2);

    response[i] = (bool) knn.find_nearest(sample, K);
    if (match_static_.at(i) == false && response[i] == true)
    {
      // this is likely a bad match => set flag to erase it
      erase_at.push_back(i);
    }
    if (match_static_.at(i) == true && response[i] == false)
    {
      match_static_.at(i) = false;
    }
  }
  // Erase the matches at flagged locations
  for(int i = erase_at.size() - 1; i >= 0; i--)
  {
    prev_pts_.erase(prev_pts_.begin() + erase_at[i]);
    next_pts_.erase(next_pts_.begin() + erase_at[i]);
    match_static_.erase(match_static_.begin() + erase_at[i]);
  }
}
