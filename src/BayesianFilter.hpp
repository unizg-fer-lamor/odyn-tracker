#include <opencv2/core/core.hpp>
#include <numeric>

#include "Angle.hpp"
#include "Odometry.hpp"

using namespace cv;

class BayesianFilter
{
private:
  Angle ang_;
  int sample_num_;

  double pdfVMF(Point3d Zk, double k, Point3d x);

  Mat sampleVMF(int n, Point3d u, double k);

  double A(double k);
  double dA(double k);
  double Ainv(double y, double guess);

  double convolveVMF(double k1, double k2);


public:
  Mat samples_;
  Point3d est_u_;
  double est_k_;
  bool initialized_;
  std::vector<double> rot_velocities_;

  BayesianFilter();
  ~BayesianFilter();

  void initialize(Point3d u, double k);
  void predict(Odometry& odom, double p_noise_k, vector<Point3d> Zk, double dt);
  void update(vector<Point3d> Zk_vec, double k, double dt);
};
