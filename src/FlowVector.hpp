#include <opencv2/core/core.hpp>

#include "Sphere.hpp"
#include "Camera.hpp"
#include "Angle.hpp"

using namespace cv;

class FlowVector
    // This class actually wraps two points on a sphere that were categorized as the same
    // in two consequtive frames, i.e. a flow vector on a sphere
{
private:
  Sphere sph_;
public:
  Point3d init_, terminal_;
  Point img_init_, img_terminal_;
  bool is_static_;
  int id_;

  FlowVector(Point3d init, Point3d terminal);

  FlowVector(Point img_init, Point img_terminal, const Camera cam, bool is_static);

  ~FlowVector();

  double getAzimuth() const;
  double getElevation() const;
  double getChordDist() const;
  double getGcDist() const;
};

class SumFlowVectorAtId
    // class sums up all the points in the FlowVector at given Id
{
private:
  int query_id_;
public:
  SumFlowVectorAtId(int i) : query_id_(i) {}
  Point3d operator() (Point3d acc, const FlowVector& fv);
};

class FlowVectorAtId
    // class defined for returnig true if argument equals the partition id of the flow vector
{
private:
  int query_id_;
public:
  FlowVectorAtId(int i) : query_id_(i) {}
  bool operator() (const FlowVector& fv);
};

class AreEqualFlowVectors
    // Class defined only for the purpose of being a binary predicate for comparison of two flow vectors
    // for the partition function. We define a thresholding comparison to decide if they belong to the
    // same group, i.e. if they are caused by the same dynamic object
{
private:
  Angle ang_;
  Sphere sph_;
public:
  bool operator() (const FlowVector& fv1, const FlowVector& fv2);
};
