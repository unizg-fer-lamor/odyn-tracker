#include <opencv2/core/core.hpp>

#include "Angle.hpp"
#include "Sphere.hpp"
#include "Camera.hpp"

class VisualServoControler
{
private:
  double v_, omega_;
  double elev_des_, azim_des_;
  bool activated_;
  double arb_depth_, gain_;
  Angle ang_;
  Sphere sph_;
  double lambda_0_, lambda_inf_, lambda_der_;
  double a_, b_, c_;

  double calculateAdaptGain(double err);

public:
  VisualServoControler();
  ~VisualServoControler();

  void setDesiredPosition(double elevation, double azimuth);
  void activate(bool flag);

  void parametrizeControler(double arb_depth, double l_0, double l_inf, double l_der);
  void safetyCheck(std::vector<Point3d> Zk);
  void calculateControlVector(Point3d est_u, Camera& cam);

  double getTransVel();
  double getRotVel();
  Point3d getDesiredPosition();
};
