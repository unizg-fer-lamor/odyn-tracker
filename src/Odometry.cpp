#include "Odometry.hpp"

// class for wrapping and handeling odometry data

Odometry::Odometry()
{
  stamp_curr_ = 0;
  x_curr_ = 0.0d;
  y_curr_ = 0.0d;
  z_curr_ = 0.0d;
  th_curr_ = 0.0d;

  stamp_prev_ = 0;
  x_prev_ = 0.0d;
  y_prev_ = 0.0d;
  z_prev_ = 0.0d;
  th_prev_ = 0.0d;

  vel_x_ = 0.0d;
  vel_th_ = 0.0d;
  robot_is_moving_ = false;
}

Odometry::~Odometry()
{
}

void Odometry::initialize()
{
  stamp_curr_ = 0;
  x_curr_ = 0.0d;
  y_curr_ = 0.0d;
  z_curr_ = 0.0d;
  th_curr_ = 0.0d;

  stamp_prev_ = 0;
  x_prev_ = 0.0d;
  y_prev_ = 0.0d;
  z_prev_ = 0.0d;
  th_prev_ = 0.0d;

  vel_x_ = 0.0d;
  vel_th_ = 0.0d;
  robot_is_moving_ = false;
  stack_.clear();
}

void Odometry::update(const unsigned long img_stamp)
// Based on the image stamp and the odometry stack we interpolate the robot position
// at the time when the image was taken. We assume constant motion between the steps.
{
  if (stack_.empty() || stack_.size() < 3)
    return;

  double img_stmp = (double) img_stamp, odom_stmp;
  int init = 0, terminal = 0;
  // let's search for elements in stack just before and just after the image message
  for (int i = 0; i < (int) stack_.size(); i++)
  {
    odom_stmp = (double) stack_[i].header.stamp.toNSec();
    if(odom_stmp < img_stmp)
      init = i;
    if(odom_stmp >= img_stmp)
    {
      terminal = i;
      break;
    }
  }

  double init_stmp, terminal_stmp, dt, dx, dy, dth, th_i, th_t;
  bool image_after = false;
  // In case image came after the latest odometry message we guess movement based on the latest available
  if (terminal == 0)
  {
    terminal = (int) stack_.size() - 1;
    init = (int) stack_.size() - 2;
    image_after = true;
  }

  init_stmp = (double) stack_[init].header.stamp.toNSec();
  terminal_stmp = (double) stack_[terminal].header.stamp.toNSec();

  if (image_after)
  {
    dt = (img_stmp - terminal_stmp) / (terminal_stmp - init_stmp);
  }
  else
  {
    dt = (img_stmp - init_stmp) / (terminal_stmp - init_stmp);
  }

  dx = dt * (stack_[terminal].pose.pose.position.x - stack_[init].pose.pose.position.x);
  dy = dt * (stack_[terminal].pose.pose.position.y - stack_[init].pose.pose.position.y);
  th_i = 2.0d * std::atan(stack_[init].pose.pose.orientation.z / stack_[init].pose.pose.orientation.w);
  th_t = 2.0d * std::atan(stack_[terminal].pose.pose.orientation.z / stack_[terminal].pose.pose.orientation.w);
  dth = ang_.subtract(th_t, th_i);
  dth = dt * dth;

  // Interpolate the current position of the robot
  if (image_after)
  {
    stamp_curr_ = img_stamp;
    x_curr_ = stack_[terminal].pose.pose.position.x + dx;
    y_curr_ = stack_[terminal].pose.pose.position.y + dy;
    z_curr_ = stack_[terminal].pose.pose.position.z;
    th_curr_ = th_t + dth;
  }
  else
  {
    stamp_curr_ = img_stamp;
    x_curr_ = stack_[init].pose.pose.position.x + dx;
    y_curr_ = stack_[init].pose.pose.position.y + dy;
    z_curr_ = stack_[init].pose.pose.position.z;
    th_curr_ = th_i + dth;
  }

  th_curr_ = ang_.assure180(th_curr_);

  // to keep the stack bounded we will be poping the first element as soon as
  // stack size is larger than a certain threshold
  if ((int) stack_.size() > 5)
    stack_.erase (stack_.begin());

  // Set flags if the robot is moving
  vel_x_ = stack_[terminal].twist.twist.linear.x;
  vel_th_ = stack_[terminal].twist.twist.angular.z;
  if (vel_x_ == 0.0d and vel_th_ == 0.0d)
  {
    robot_is_moving_ = false;
  }
  else
  {
    robot_is_moving_ = true;
  }

  // Initializing the previous robot's position
  if ((int) stack_.size() == 3)
  {
    stamp_prev_ = stack_[init].header.stamp.toNSec();
    x_prev_ = stack_[init].pose.pose.position.x;
    y_prev_ = stack_[init].pose.pose.position.y;
    z_prev_ = stack_[init].pose.pose.position.z;
    th_prev_ = th_i;
  }

}

Point3d Odometry::tfToCurrentFrame(Point3d pt)
{
  Mat pt_hom, T, R;
  Point3d pt_tf;
  // the argument point will be transformed to the displaced coo. system (from prev to curr)
  pt_hom = (Mat_<double>(4,1) << pt.x, pt.y, pt.z, 1.0d);
  // Rotation of the image frame to the robot frame
  R = (Mat_<double>(4,4) <<
       cos(-M_PI / 2.0d), -sin(-M_PI / 2.0d), 0.0d, 0.0d,
       sin(-M_PI / 2.0d), cos(-M_PI / 2.0d), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  pt_hom = R * pt_hom;
  // Rotation and translation from the previous robot frame to world frame
  R = (Mat_<double>(4,4) <<
       cos(th_prev_), -sin(th_prev_), 0.0d, 0.0d,
       sin(th_prev_), cos(th_prev_), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  T = (Mat_<double>(4,4) <<
       1.0d, 0.0d, 0.0d, x_prev_,
       0.0d, 1.0d, 0.0d, y_prev_,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  pt_hom = T * R * pt_hom;
  // Rotation and translation from the world frame to the current robot frame
  R = (Mat_<double>(4,4) <<
       cos(-th_curr_), -sin(-th_curr_), 0.0d, 0.0d,
       sin(-th_curr_), cos(-th_curr_), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  T = (Mat_<double>(4,4) <<
       1.0d, 0.0d, 0.0d, -x_curr_,
       0.0d, 1.0d, 0.0d, -y_curr_,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  pt_hom = R * T * pt_hom;
  // Rotation of the robot frame to the image frame
  R = (Mat_<double>(4,4) <<
       cos(M_PI / 2.0d), -sin(M_PI / 2.0d), 0.0d, 0.0d,
       sin(M_PI / 2.0d), cos(M_PI / 2.0d), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  pt_hom = R * pt_hom;
  pt_tf.x = pt_hom.at<double>(0,0);
  pt_tf.y = pt_hom.at<double>(1,0);
  pt_tf.z = pt_hom.at<double>(2,0);

  pt_tf = pt_tf * (1.0d / norm(pt_tf));

  return pt_tf;
}

Point3d Odometry::justRotateForDisplacement(Point3d pt)
{
  Mat pt_hom, R;
  Point3d pt_tf;
  double th_rot;
  pt_hom = (Mat_<double>(4,1) << pt.x, pt.y, pt.z, 1.0d);

  th_rot = ang_.subtract(th_curr_, th_prev_);
  R = (Mat_<double>(4,4) <<
       cos(-th_rot), -sin(-th_rot), 0.0d, 0.0d,
       sin(-th_rot), cos(-th_rot), 0.0d, 0.0d,
       0.0d, 0.0d, 1.0d, 0.0d,
       0.0d, 0.0d, 0.0d, 1.0d);
  pt_hom = R * pt_hom;
  pt_tf.x = pt_hom.at<double>(0,0);
  pt_tf.y = pt_hom.at<double>(1,0);
  pt_tf.z = pt_hom.at<double>(2,0);

  return pt_tf;
}
