#include <stdio.h>
#include <algorithm>
#include <numeric>
#include <functional>
#include <ctime>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/TwistStamped.h>
#include <message_filters/subscriber.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <turtlesim/Velocity.h>
#include <omnicam_dynobj_tracking/EstimatedPointOnSphereStamped.h>

#include "../src/Angle.hpp"
#include "../src/Sphere.hpp"
#include "../src/Camera.hpp"
#include "../src/FlowVector.hpp"
#include "../src/Odometry.hpp"
#include "../src/Features.hpp"
#include "../src/BayesianFilter.hpp"
#include "../src/VisualServoControler.hpp"

#define _USE_MATH_DEFINES

using namespace cv;
namespace enc = sensor_msgs::image_encodings;

/* --------------------------------Histogram-------------------------------- */
/* ----------------------------------begin---------------------------------- */
void contrastStreching(Mat& img, const Mat& hist, const Mat& mask)
{
  int a = 0, b = 255, c, d, pix_strech;
  double min_val, max_val;
  int min_idx, max_idx;
  minMaxIdx(hist, &min_val, &max_val, &min_idx, &max_idx, noArray());;
  d = max_idx * 0.97;
  c = max_idx * 0.05;
  // Do contrast streching with Iout = (Iin - c)*(b-a)/(d-c) + a
  for (int i = 0; i < img.rows ; i++)
  {
    for (int j = 0; j < img.cols; j++)
    {
      if ((int) mask.at<uchar>(i,j) == 255)
      {
        pix_strech = (((int) img.at<uchar>(i,j) - c) * (b - a) / (d - c) + a);
        if (pix_strech > 255)
        {
          pix_strech = 255;
        }
        else if (pix_strech < 0)
        {
          pix_strech = 0;
        }
        img.at<uchar>(i,j) = (uchar) pix_strech;
      }
    }
  }
}

void showHistogram(const Mat& hist)
{
  int bins = 256;// number of bins
  int nc = 1;    // number of channels

  // Obtain the maximum (peak) value
  // Needed to normalize the display later
  int hmax = 0;
  for (int i = 0; i < bins-1; i++)
    hmax = (int) hist.at<float>(i,0) > hmax ? (int) hist.at<float>(i,0) : hmax;

  const char* wname[3] = { "blue", "green", "red" };
  Scalar colors[3] = { Scalar(255,0,0), Scalar(0,255,0), Scalar(0,0,255) };

  vector<Mat> canvas(nc);

  // Display each histogram in a canvas
  for (int i = 0; i < nc; i++)
  {
    canvas[i] = Mat::ones(125, bins, CV_8UC3);

    for (int j = 0, rows = canvas[i].rows; j < bins-1; j++)
    {
      line(
            canvas[i],
            Point(j, rows),
            Point(j, rows - ((int) hist.at<float>(j,0) * rows/hmax)),
            nc == 1 ? Scalar(200,200,200) : colors[i],
            1, 8, 0
            );
    }

    imshow(nc == 1 ? "Histogram" : wname[i], canvas[i]);
  }
}

/* --------------------------------Histogram-------------------------------- */
/* -----------------------------------end----------------------------------- */

static const char WINDOW[] = "Image window";

class CheckIf
    // class defined for returning true if the boolean valued corresponds
{
private:
  int check_if_;
public:
  CheckIf(bool i) : check_if_(i) {}
  bool operator() (const bool vector_member)
  {
    return (vector_member == check_if_);
  }
};

class AreEqualZk
{
private:
  Angle ang_;
public:
  bool operator() (const Point3d zi, const Point3d zj)
  {
    float az_i, az_j, az_diff;
    az_i = atan2(zi.y, zi.x);
    az_j = atan2(zj.y, zj.x);

    az_diff = fabs(ang_.subtract(az_i, az_j));

    float az_thresh = ang_.degToRad(15.0f);
    if (az_diff < az_thresh)
      return true;
    else
      return false;
  }
};

class OmniCamTracking
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Subscriber odom_sub_, joy_sub_;
  ros::Publisher cmd_vel_pub_, cmd_vel_theory_pub_, turtle_vel_pub_, est_point_sphere_pub_, zk_point_sphere_pub_;

  Features ftrs_;
  Camera cam_;
  Mat mask_;
  Mat prev_image_;
  Odometry odom_;
  BayesianFilter bf_;
  VisualServoControler vs_ctrl_;
  Angle ang_;

public:
  OmniCamTracking()
    :  it_(nh_)
  {
    image_sub_ = it_.subscribe("camera/image_rect", 10, &OmniCamTracking::ocamTrackCb, this);
    image_pub_ = it_.advertise("image_optical_flow", 1);
    
    odom_sub_ = nh_.subscribe("RosAria/pose", 10, &OmniCamTracking::odometryCb, this);
    joy_sub_ = nh_.subscribe("/joy", 1, &OmniCamTracking::joyCb, this);
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/RosAria/cmd_vel", 1);
    cmd_vel_theory_pub_ = nh_.advertise<geometry_msgs::TwistStamped>("ocam_tracking/cmd_vel_theory", 1);
    turtle_vel_pub_ = nh_.advertise<turtlesim::Velocity>("turtle1/command_velocity", 1);

    est_point_sphere_pub_ = nh_.advertise<omnicam_dynobj_tracking::EstimatedPointOnSphereStamped>
        ("ocam_tracking/est_point", 1);
    zk_point_sphere_pub_ = nh_.advertise<omnicam_dynobj_tracking::EstimatedPointOnSphereStamped>
        ("ocam_tracking/zk_point", 1);

    namedWindow(WINDOW, CV_WINDOW_AUTOSIZE);
  }

  ~OmniCamTracking()
  {
    destroyWindow(WINDOW);
  }

  void joyCb(const sensor_msgs::Joy& msg_joy)
  {
    if (msg_joy.buttons[0] == 1)
    {
      vs_ctrl_.activate(true);
      ROS_INFO("[ocam_tracking]: Control activated");
    }
    else
    {
      vs_ctrl_.activate(false);
      ROS_INFO("[ocam_tracking]: Control deactivated");
    }
  }

  void odometryCb(const nav_msgs::Odometry::ConstPtr& msg_pose)
  {
    nav_msgs::Odometry odom_aux;
    // copy the header data
    odom_aux.header.stamp = msg_pose->header.stamp;
    odom_aux.header.frame_id = msg_pose->header.frame_id;
    
    // copy the position data
    odom_aux.pose.pose.position.x = msg_pose->pose.pose.position.x;
    odom_aux.pose.pose.position.y = msg_pose->pose.pose.position.y;
    odom_aux.pose.pose.position.z = msg_pose->pose.pose.position.z;
    odom_aux.pose.pose.orientation = msg_pose->pose.pose.orientation;
    
    // copy the velocity data
    odom_aux.child_frame_id = msg_pose->child_frame_id;
    odom_aux.twist.twist.linear.x = msg_pose->twist.twist.linear.x;
    odom_aux.twist.twist.linear.y = msg_pose->twist.twist.linear.y;
    odom_aux.twist.twist.angular.z = msg_pose->twist.twist.angular.z;
    
    // Push the data into the stack
    odom_.stack_.push_back(odom_aux);
    
    // Keeping the odometry stack size bounded if for some reason stack is not maintained by the tracker
    if (odom_.stack_.size() == 21)
    {
      ROS_WARN("Size of the odometry stack has exceeded 20! It seems it is not maintainted by image callback.");
    }
    else if (odom_.stack_.size() == 50)
    {
      ROS_WARN("Size of the odometry stack is 50. Clearing the stack!");
      odom_.initialize();
    }
  }

  void ocamTrackCb(const sensor_msgs::ImageConstPtr& msg)
  {
//    double begin = ros::Time::now().toNSec();//debug
    Mat cv_image, cv_image_masked;
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      // Convert the ROS image to OpenCV image
      cv_ptr = cv_bridge::toCvCopy(msg, enc::MONO8);
      cv_image = cv_ptr->image;
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("[ocam_tracking]: cv_bridge exception: %s", e.what());
      return;
    }
    
    // Create/apply the mask to received image
    ftrs_.stamp_curr_ = msg->header.stamp.toNSec();
    if (ftrs_.prev_pts_.empty())
    {
      mask_.create(cv_image.rows, cv_image.cols, CV_8UC1);
      for(int i = 0; i < cv_image.rows; i++)
      {
        for(int j = 0; j < cv_image.cols; j++)
        {
          double dx = i - cam_.K_.at<double>(1,2);
          double dy = j - cam_.K_.at<double>(0,2);
          if(sqrt(dx*dx + dy*dy) < 235)
          {
            mask_.at<unsigned char>(i,j) = 255;
          }
          else
          {
            mask_.at<unsigned char>(i,j) = 0;
          }
        }
      }
      cv_image.copyTo(cv_image_masked, mask_);
    }
    else
    {
      cv_image.copyTo(cv_image_masked, mask_);
    }
    cv_image = cv_image_masked;

    // Perform contrast streching in the image
    int hbins = 256, himage_num = 1, hist_dim = 1;
    int histSize[] = {hbins};
    float hranges[] = { 0, 256 };
    const float* ranges[] = { hranges };
    Mat hist;
    int channels[] = {0};

    calcHist(&cv_image, himage_num, channels, mask_, hist, hist_dim, histSize, ranges);
    contrastStreching(cv_image, hist, mask_);
    //    calcHist(&cv_image, himage_num, channels, mask_, hist, hist_dim, histSize, ranges);
    //    showHistogram(hist);

    if (ftrs_.prev_pts_.empty())
    {
      // detect features in the image (stores result in _query members)
      ftrs_.detect(cv_image, mask_, odom_.robot_is_moving_);

      // in the first iteration just copy the extracted features
      ftrs_.stamp_prev_ = msg->header.stamp.toNSec();
      cv_image.copyTo(prev_image_);

      ftrs_.prev_pts_.clear();
      Point2f pt;
      for (int i = 0; i < (int) ftrs_.pts_detected_.size(); i++)
      {
        pt.x = ftrs_.pts_detected_[i].pt.x;
        pt.y = ftrs_.pts_detected_[i].pt.y;
        ftrs_.prev_pts_.push_back(pt);
      }

      return;
    }
    
    // LK Pyramidal optical flow
    vector<uchar> status;
    vector<float> error;
    ftrs_.next_pts_.clear();
    calcOpticalFlowPyrLK(prev_image_, cv_image, ftrs_.prev_pts_, ftrs_.next_pts_, status, error);

    // update (interpolate) the odometry with image time stamp and then detect moving features
    odom_.update(ftrs_.stamp_curr_);
    ftrs_.detectMovement(cam_, odom_);

    // Check if most of the features are dynamic (crazy bug, don't know why it happens)
    float dyn_num, static_num;
    bool dynamic_dominant;
    static_num = count_if(ftrs_.match_static_.begin(), ftrs_.match_static_.end(), CheckIf(true));
    dyn_num = count_if(ftrs_.match_static_.begin(), ftrs_.match_static_.end(), CheckIf(false));
    dynamic_dominant = (dyn_num / (dyn_num + static_num) > 0.4f) ? true : false;

    int num_partitions;
    std::vector<int> ids;
    std::vector<FlowVector> odyn_vfield;
    std::vector<Point3d> Zk, Zk_filt;

    if (!dynamic_dominant)
    {
      // Transform all the matches into a vector of FlowVector objects
      Point pt_img, pt_matched_img;
      for (int i = 0; i < (int) ftrs_.prev_pts_.size(); i++)
      {
        // Push only dynamic vectors which are henceforth the only ones considered
        if (!ftrs_.match_static_.at(i))
        {
          pt_img = ftrs_.prev_pts_.at(i);
          pt_matched_img = ftrs_.next_pts_.at(i);

          FlowVector fv(pt_img, pt_matched_img, cam_, true);
          odyn_vfield.push_back(fv);
        }
      }
      // partition all the flow vectors into groups that consist of similar vectors (presumably they come from
      // the same dynamic object)
      num_partitions = partition(odyn_vfield, ids, AreEqualFlowVectors());

      // copy the results of the partitioning into each object in odyn_vfield
      for (int i = 0; i < (int) ids.size(); i++)
      {
        odyn_vfield[i].id_ = ids[i];
      }

      // remove all the groups that contain insignificant number of vectors (most likely they are outliers), otherwise
      // generate a measurement vector
      int count_ids;
      int removed_partitions = 0;

      Point3d pt_acc;
      for (int i = 0; i < num_partitions; i++)
      {
        count_ids = count_if(odyn_vfield.begin(), odyn_vfield.end(), FlowVectorAtId(i));
        if (count_ids < 20)
        {
          odyn_vfield.erase( remove_if(odyn_vfield.begin(), odyn_vfield.end(), FlowVectorAtId(i)), odyn_vfield.end() );
          removed_partitions++;
        }
        else
        {
          pt_acc.x = 0.0d; pt_acc.y = 0.0d; pt_acc.z = 0.0d;
          pt_acc = accumulate(odyn_vfield.begin(), odyn_vfield.end(), pt_acc, SumFlowVectorAtId(i));
          Zk.push_back(pt_acc * (1.0d / norm(pt_acc)));
        }
      }
      num_partitions = num_partitions - removed_partitions;

      // Filter out the Zk (if two of them are with similar azimuth then they are likely from the same object
      ids.clear();
      int num_Zk_partitions;
      double nrm;
      num_Zk_partitions = partition(Zk, ids, AreEqualZk());
      for (int i = 0; i < num_Zk_partitions; i++)
      {
        pt_acc.x = 0.0d; pt_acc.y = 0.0d; pt_acc.z = 0.0d;
        for (int j = 0; j < (int) Zk.size(); j++)
        {
          if (i == ids[j])
            pt_acc = pt_acc + Zk[i];
        }
        nrm = norm(pt_acc);
        pt_acc.x = pt_acc.x / nrm;
        pt_acc.y = pt_acc.y / nrm;
        pt_acc.z = pt_acc.z / nrm;
        Zk_filt.push_back(pt_acc);
      }
      Zk = Zk_filt;
    }

    // Filtering and tracking with von Mises-Fisher
    double dt = ((double) ftrs_.stamp_curr_ - (double) ftrs_.stamp_prev_) * 1e-9; // since it is in ns
    // if there is some measurement and there is only one (since we are tracking just one dynamic object)
    if (!Zk.empty() && Zk.size() == 1 && !bf_.initialized_)
    {
      // initialize Bayesian filter
      bf_.initialize(Zk[0], 500.0d);

      // once the tracker starts we start the control too
      vs_ctrl_.setDesiredPosition(ang_.degToRad(65.0d), ang_.degToRad(-90.0d));
      vs_ctrl_.parametrizeControler(0.025d, 0.3d * 1.666d, 0.3d * 0.1666d, 0.3d * 1.666d);
    }

    if (bf_.initialized_)
    {
      bf_.predict(odom_, 5000.0d, Zk, dt);
      bf_.update(Zk, 100.0d, dt);
    }
//    double begin = ros::Time::now().toNSec();
//    std::cout << "Exec. time: " << (ros::Time::now().toNSec() - begin)*1e-9 << std::endl;

    vs_ctrl_.calculateControlVector(bf_.est_u_, cam_);

    // theoretical commands
    geometry_msgs::TwistStamped cmd_theory;
    cmd_theory.header.stamp = ros::Time::now();
    cmd_theory.header.frame_id = "/odom";
    cmd_theory.twist.linear.x = vs_ctrl_.getTransVel();
    cmd_theory.twist.linear.y = 0.0d; cmd_theory.twist.linear.z = 0.0d;
    cmd_theory.twist.angular.z = vs_ctrl_.getRotVel();
    cmd_theory.twist.angular.x = 0.0d; cmd_theory.twist.angular.y = 0.0d;
    cmd_vel_theory_pub_.publish(cmd_theory);

    vs_ctrl_.safetyCheck(Zk);

    // It is safe to assign the values here since safety check sets velocities to zero if control is not active
    geometry_msgs::Twist cmd;
    cmd.linear.x = vs_ctrl_.getTransVel();
    cmd.linear.y = 0.0d; cmd.linear.z = 0.0d;
    cmd.angular.z = vs_ctrl_.getRotVel();
    cmd.angular.x = 0.0d; cmd.angular.y = 0.0d;
    cmd_vel_pub_.publish(cmd);

    turtlesim::Velocity turtle_vel;
    turtle_vel.linear = vs_ctrl_.getTransVel();
    turtle_vel.angular = vs_ctrl_.getRotVel();
    turtle_vel_pub_.publish(turtle_vel);

    /* --------------------------------Vizualization-------------------------------- */
    /* -----------------------------------begin------------------------------------- */
    // draw the keypoints on the image
    Mat image_keypoints;
    std::vector<KeyPoint> dummy; // to prevent drawing with drawKeypoints
    int thickness = 2;
    drawKeypoints(cv_image, dummy, image_keypoints, CV_RGB(255,0,0)); // just to get color on image_keypoints
    
    Point2d center;
    center.x = cam_.K_.at<double>(0,2);
    center.y = cam_.K_.at<double>(1,2);
    if (!ftrs_.prev_pts_.empty() && !dynamic_dominant)
    {

      //      circle(image_keypoints, center, 235, CV_RGB(0,255,0), 1);//debug
      for (int i = 0; i < (int) ftrs_.prev_pts_.size(); i++)
      {

        if (ftrs_.match_static_.at(i))
        {
          // draw the static matched keypoints and connect them with a line
          circle(image_keypoints, ftrs_.next_pts_.at(i), 1, CV_RGB(0,0,255), thickness);
          line(image_keypoints, ftrs_.next_pts_.at(i), ftrs_.prev_pts_.at(i), CV_RGB(0,255,0), thickness);
        }
      }
      
      
      for (int i = 0; i < (int) odyn_vfield.size(); i++)
      {
        //        int color = (int) ((float) odyn_vfield[i].id_ * (255.0f / (float) 5));
        circle(image_keypoints, odyn_vfield[i].img_terminal_, 1, CV_RGB(0,0,255), thickness);
        line(image_keypoints, odyn_vfield[i].img_init_, odyn_vfield[i].img_terminal_, CV_RGB(255,0,0), thickness);
      }
      
      for (int i = 0; i < (int) Zk.size(); i++)
      {
        circle(image_keypoints, cam_.sphere2Cam(Zk[i]), 3, CV_RGB(255,255,0), 5);
      }

    }
    
    Point3d part;
    for (int i = 0; i < bf_.samples_.cols; i++)
    {
      part.x = bf_.samples_.at<double>(0,i);
      part.y = bf_.samples_.at<double>(1,i);
      part.z = bf_.samples_.at<double>(2,i);
      circle(image_keypoints, cam_.sphere2Cam(part), 1, CV_RGB(0,255,0), thickness);
    }
    circle(image_keypoints, cam_.sphere2Cam(bf_.est_u_), 3, CV_RGB(255,20,147), 5);

    // Draw the axes on the image
    Point3d x_axis(1.0d, 0.0d, 0.0d), y_axis(0.0d, 1.0d, 0.0d), z_axis(0.0d, 0.0d, 1.0d);
    circle(image_keypoints, cam_.sphere2Cam(x_axis), 3, CV_RGB(255,0,0), 5);
    circle(image_keypoints, cam_.sphere2Cam(y_axis), 3, CV_RGB(0,255,0), 5);
    circle(image_keypoints, cam_.sphere2Cam(z_axis), 3, CV_RGB(0,0,255), 5);

    // Draw the desired position on the image
    circle(image_keypoints, cam_.sphere2Cam(vs_ctrl_.getDesiredPosition()), 3, CV_RGB(87,34,176), 5);
    
    // write the number of keypoints on the image
    std::string text = "[#keypts]: ";
    char buffer[10];
    sprintf(buffer, "%d", (int) ftrs_.prev_pts_.size());
    putText(image_keypoints, text + buffer, Point(15,15), FONT_HERSHEY_COMPLEX, 0.5,
            CV_RGB(255,255,255), 1);

    text = "[#est az(u)]: ";
    sprintf(buffer, "%d", (int) (ang_.radToDeg(atan2(bf_.est_u_.y, bf_.est_u_.x))));
    putText(image_keypoints, text + buffer, Point(15,35), FONT_HERSHEY_COMPLEX, 0.5,
            CV_RGB(255,255,255), 1);

    text = "[#est k]: ";
    sprintf(buffer, "%d", (int) bf_.est_k_);
    putText(image_keypoints, text + buffer, Point(15,55), FONT_HERSHEY_COMPLEX, 0.5,
            CV_RGB(255,255,255), 1);
    
    imshow(WINDOW, image_keypoints);
    waitKey(3); // give time to imshow    
    /* --------------------------------Vizualization-------------------------------- */
    /* ------------------------------------end-------------------------------------- */
    
    omnicam_dynobj_tracking::EstimatedPointOnSphereStamped est_point;
    est_point.header.stamp = ros::Time::now();
    est_point.header.frame_id = "/odom";
    est_point.mean.x = bf_.est_u_.x;
    est_point.mean.y = bf_.est_u_.y;
    est_point.mean.z = bf_.est_u_.z;
    est_point.kappa = bf_.est_k_;

    est_point_sphere_pub_.publish(est_point);

    omnicam_dynobj_tracking::EstimatedPointOnSphereStamped zk_point;
    zk_point.header.stamp = ros::Time::now();
    zk_point.header.frame_id = "/odom";
    if (!Zk.empty())
    {
      zk_point.mean.x = Zk[0].x;
      zk_point.mean.y = Zk[0].y;
      zk_point.mean.z = Zk[0].z;
    }
    else
    {
      zk_point.mean.x = 0;
      zk_point.mean.y = 0;
      zk_point.mean.z = 0;
    }
    zk_point.kappa = 0;

    zk_point_sphere_pub_.publish(zk_point);


    // convert OpenCV image to ROS message and publish it
    sensor_msgs::Image ros_image;
    cv_bridge::CvImage cv_brimage;
    cv_brimage.header.stamp = ros::Time::now();
    cv_brimage.header.frame_id = "image";
    cv_brimage.encoding = "bgr8";
    cv_brimage.image = image_keypoints;
    cv_brimage.toImageMsg(ros_image);
    image_pub_.publish(ros_image);
    
    // features from the current frame will be previous in the next frame
    ftrs_.stamp_prev_ = ftrs_.stamp_curr_;
    ftrs_.prev_pts_.clear();
    bool reinit_tracker = true; // we reinit the tracker at each step
    if (reinit_tracker)
    {
      // detect features in the image (stores result in _query members)
      ftrs_.detect(cv_image, mask_, odom_.robot_is_moving_);

      Point2f pt;
      for (int i = 0; i < (int) ftrs_.pts_detected_.size(); i++)
      {
        pt.x = ftrs_.pts_detected_[i].pt.x;
        pt.y = ftrs_.pts_detected_[i].pt.y;
        ftrs_.prev_pts_.push_back(pt);
      }
    }
    else
    {
      ftrs_.prev_pts_ = ftrs_.next_pts_;
    }
    cv_image.copyTo(prev_image_);

    if ((int) odom_.stack_.size() >= 3)
    {
      odom_.stamp_prev_ = odom_.stamp_curr_;
      odom_.x_prev_ = odom_.x_curr_;
      odom_.y_prev_ = odom_.y_curr_;
      odom_.z_prev_ = odom_.z_curr_;
      odom_.th_prev_ = odom_.th_curr_;
    }
  }
};

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "ocam_tracking");
  OmniCamTracking ocam_tracking;

  ROS_INFO("[ocam_tracking]: About to spin");
  ros::spin();

  return 0;
}
