#include <stdio.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <message_filters/subscriber.h>
#include <nav_msgs/Odometry.h>
#include <turtlesim/Velocity.h>

class JoyControl
{
private:
  ros::NodeHandle nh_;
  ros::Subscriber joy_sub_, odom_sub_;
  ros::Publisher cmd_vel_pub_, turtle_vel_pub_;
  geometry_msgs::Twist cmd_;
  turtlesim::Velocity turtle_vel_;
  bool control_active_;
  double linear_vel_, angular_vel_;

public:
  JoyControl()
  {
    joy_sub_ = nh_.subscribe("/joy", 1, &JoyControl::joyCb, this);
    odom_sub_ = nh_.subscribe("RosAria/pose", 1, &JoyControl::odometryCb, this);
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/RosAria/cmd_vel", 1);
    turtle_vel_pub_ = nh_.advertise<turtlesim::Velocity>("turtle1/command_velocity", 1);

    control_active_ = false;
    cmd_.linear.x = 0.0d; cmd_.linear.y = 0.0d; cmd_.linear.z = 0.0d;
    cmd_.angular.x = 0.0d; cmd_.angular.y = 0.0d; cmd_.angular.z = 0.0d;

    turtle_vel_.linear = 0.0d; turtle_vel_.angular = 0.0d;
    linear_vel_ = 0.0d; angular_vel_ = 0.0d;
  }

  ~JoyControl()
  {
  }

  void odometryCb(const nav_msgs::Odometry::ConstPtr& msg_pose)
  {
    cmd_vel_pub_.publish(cmd_);
    turtle_vel_pub_.publish(turtle_vel_);
  }

  void joyCb(const sensor_msgs::Joy& msg_joy)
  {
    if (msg_joy.buttons[0] == 1)
    {
      control_active_ = true;

//      ROS_INFO("[ocam_tracking]: Control activated");
      if (msg_joy.axes[7] == 1.0)
      {
        ROS_INFO("Move forward");
        linear_vel_ = 0.2d;
      }
      else if (msg_joy.axes[7] == -1.0)
      {
        ROS_INFO("Move backwards");
        linear_vel_ = -0.2d;
      }
      else
      {
        linear_vel_ = 0.0d;
      }

      if (msg_joy.axes[6] == 1.0)
      {
        ROS_INFO("Rotate positive");
        angular_vel_ = 0.25d;
      }
      else if (msg_joy.axes[6] == -1.0)
      {
        ROS_INFO("Rotate negative");
        angular_vel_ = -0.25d;
      }
      else
      {
        angular_vel_ = 0.0d;
      }
    }
    else
    {
      control_active_ = false;
      ROS_INFO("[ocam_tracking]: Control deactivated");
    }

    if (control_active_)
    {
      cmd_.linear.x = linear_vel_;
      cmd_.angular.z = angular_vel_;

      turtle_vel_.linear = linear_vel_;
      turtle_vel_.angular = angular_vel_;

      cmd_vel_pub_.publish(cmd_);
      turtle_vel_pub_.publish(turtle_vel_);
    }
    else
    {
      cmd_.linear.x = 0.0d; cmd_.linear.y = 0.0d; cmd_.linear.z = 0.0d;
      cmd_.angular.x = 0.0d; cmd_.angular.y = 0.0d; cmd_.angular.z = 0.0d;

      cmd_vel_pub_.publish(cmd_);

      turtle_vel_.linear = 0.0d; turtle_vel_.angular = 0.0d;
      turtle_vel_pub_.publish(turtle_vel_);
    }
  }
};

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "joy_control");
  JoyControl joy_control;

  ROS_INFO("[joy_control]: About to spin");
  ros::spin();

  return 0;
}
