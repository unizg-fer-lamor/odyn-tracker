# README #

This code is the result of joint work between the Laboratory for Autonomous Systems and Mobile Robotics of the University of Zagreb and the Lagadic group of Inria Rennes Bretagne-Atlantique. It was supported by the FP7 REGPOT project ACROSS - Centre of Research Excellence for Advanced Cooperative Systems.

The code in the repository (at a certain version) was used to obtain the results for the following paper. If you find it to be useful for your work, please cite the paper as follows: 

~~~~
@inproceedings{Markovic2014,
author = {Markovi{\'{c}}, I. and Chaumette, F. and Petrovi{\'{c}}, I.},
booktitle = {International Conference on Robotics and Automation (ICRA)},
title = {{Moving object detection, tracking and following using an omnidirectional camera on a mobile robot}},
year = {2014}
}
~~~~